package Practicas;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.*;
import javax.swing.JTextField;


public class Practica1 extends JFrame implements ActionListener{
    JLabel etiq1 = new JLabel();
    JTextField nombre = new JTextField(100);
    JButton btn ;
    
    Practica1(){
        this.setTitle("Saludador automatico");
        this.setSize(300, 200);
        this.setLayout(new FlowLayout());
        
        etiq1 = new JLabel("Escribe un nombre para saludar");
        nombre = new JTextField(20);
        btn = new JButton("Saludar");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        this.add(etiq1);
        this.add(nombre);
        this.add(btn);
        btn.addActionListener(this);
    }
    
    
    
    //this is the main 
    public static void main(String[] args) {
     
        
        
        java.awt.EventQueue.invokeLater(new Runnable(){
            @Override
            public void run() {
                new Practica1().setVisible(true);
            }
        
    });
                
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        
        JOptionPane.showMessageDialog(this,"Hola "+this.nombre.getText());
    }
}